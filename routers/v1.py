# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2025 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed Apache-2.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from fastapi import APIRouter

from apps.views import level, login, position, dept, role, menu, role_menu, user, city, notice, item, item_cate, link, \
    ad_sort, ad, member_level, member, dicts, dicts_data, config, config_data, config_web, index, upload

# 创建路由
v1 = APIRouter()
# 系统登录
v1.include_router(login.router, prefix="", tags=["系统登录"])
# 系统主页
v1.include_router(index.router, prefix="/index", tags=["系统主页"])
# 上传文件
v1.include_router(upload.router, prefix="/upload", tags=["上传文件"])
# 职级管理
v1.include_router(level.router, prefix="/level", tags=["职级管理"])
# 岗位管理
v1.include_router(position.router, prefix="/position", tags=["岗位管理"])
# 部门管理
v1.include_router(dept.router, prefix="/dept", tags=["部门管理"])
# 角色管理
v1.include_router(role.router, prefix="/role", tags=["角色管理"])
# 角色菜单
v1.include_router(role_menu.router, prefix="/rolemenu", tags=["角色菜单"])
# 菜单管理
v1.include_router(menu.router, prefix="/menu", tags=["菜单管理"])
# 用户管理
v1.include_router(user.router, prefix="/user", tags=["用户管理"])
# 城市管理
v1.include_router(city.router, prefix="/city", tags=["城市管理"])
# 通知公告
v1.include_router(notice.router, prefix="/notice", tags=["通知公告"])
# 站点管理
v1.include_router(item.router, prefix="/item", tags=["站点管理"])
# 栏目管理
v1.include_router(item_cate.router, prefix="/itemcate", tags=["栏目管理"])
# 友链管理
v1.include_router(link.router, prefix="/link", tags=["友链管理"])
# 广告位管理
v1.include_router(ad_sort.router, prefix="/adsort", tags=["广告位管理"])
# 广告管理
v1.include_router(ad.router, prefix="/ad", tags=["广告管理"])
# 会员等级管理
v1.include_router(member_level.router, prefix="/memberlevel", tags=["会员等级管理"])
# 会员管理
v1.include_router(member.router, prefix="/member", tags=["会员管理"])
# 字典管理
v1.include_router(dicts.router, prefix="/dict", tags=["字典管理"])
# 字典项管理
v1.include_router(dicts_data.router, prefix="/dictdata", tags=["字典项管理"])
# 配置管理
v1.include_router(config.router, prefix="/config", tags=["配置管理"])
# 配置项管理
v1.include_router(config_data.router, prefix="/configdata", tags=["配置项管理"])
# 网站配置
v1.include_router(config_web.router, prefix="/configweb", tags=["网站配置"])
